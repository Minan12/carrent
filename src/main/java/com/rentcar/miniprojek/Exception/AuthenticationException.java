package com.rentcar.miniprojek.Exception;

public class AuthenticationException extends RuntimeException {
    public AuthenticationException(String message) {

        super(message);
    }
}
