package com.rentcar.miniprojek.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.Collections;

@RestControllerAdvice
public class BadRequestControllerAdvice{
    @ExceptionHandler(BadRequestException.class)
    ResponseEntity<ApiExceptionResponse> handleBadRequestException(BadRequestException exception){
        final ApiExceptionResponse response = new ApiExceptionResponse(false,
                Collections.singletonList(exception.getErrorMessage()), HttpStatus.BAD_REQUEST, LocalDateTime.now());

        return ResponseEntity.status(response.getStatus()).body(response);
    }
}
