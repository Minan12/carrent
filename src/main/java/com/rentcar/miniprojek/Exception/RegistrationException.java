package com.rentcar.miniprojek.Exception;

import lombok.Data;

import java.util.List;

@Data
public class RegistrationException extends RuntimeException {
    private final List<String> errorMessages;

    public RegistrationException(List<String> errorMessages) {
        this.errorMessages = errorMessages;
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }
}
