package com.rentcar.miniprojek.Exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResourceNotFoundException extends RuntimeException{
    private final String errorMessage;
}
