package com.rentcar.miniprojek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniprojekApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniprojekApplication.class, args);
	}

}
