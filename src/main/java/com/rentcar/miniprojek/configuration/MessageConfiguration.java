package com.rentcar.miniprojek.configuration;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import static com.rentcar.miniprojek.util.ProjectConstant.DEFAULT_ENCODING;

@Configuration
public class MessageConfiguration {
    @Bean
    MessageSource exceptionMessageSource() {
        final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:/ExceptionMessages");
        messageSource.setDefaultEncoding(DEFAULT_ENCODING);

        return messageSource;
    }

}
