package com.rentcar.miniprojek.controller;


import com.rentcar.miniprojek.Exception.RegistrationException;
import com.rentcar.miniprojek.dto.customer.CustomerRequest;
import com.rentcar.miniprojek.dto.customer.CustomerResponse;
import com.rentcar.miniprojek.entity.Customer;
import com.rentcar.miniprojek.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/CarRent")
@Validated
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService){

        this.customerService = customerService;
    }

    @PostMapping("/regist-cust")
    public ResponseEntity<CustomerResponse> addCustomer(@Valid @RequestBody CustomerRequest request) throws RegistrationException {
        CustomerResponse response = customerService.saveCustomer(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<CustomerResponse> getById(@PathVariable("id") Integer id) {
        CustomerResponse response = customerService.getById(id);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/update-customer/{id}")
    public ResponseEntity<CustomerResponse> updateCustomer(@PathVariable("id") Integer id,
                                                           @RequestBody CustomerRequest request) {
        CustomerResponse response = customerService.updateCustomer(id, request);
        return ResponseEntity.ok(response);
    }

}