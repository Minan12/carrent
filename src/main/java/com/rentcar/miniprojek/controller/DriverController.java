package com.rentcar.miniprojek.controller;

import com.rentcar.miniprojek.dto.driver.DriverRequest;
import com.rentcar.miniprojek.dto.driver.DriverResponse;
import com.rentcar.miniprojek.service.DriverService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/driver")
@RequiredArgsConstructor
public class DriverController {
    private final DriverService driverService;

    @PostMapping("/add")
    public ResponseEntity<DriverResponse> addDriver(@RequestBody DriverRequest driverRequest){
        DriverResponse driverResponse = driverService.saveDriver(driverRequest);
        return new ResponseEntity<>(driverResponse, HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public ResponseEntity<List<DriverResponse>> getAllDriver() {
        List<DriverResponse> driverList = driverService.getAllDriver();
        return ResponseEntity.ok(driverList);
    }

}
