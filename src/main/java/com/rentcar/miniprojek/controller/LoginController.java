package com.rentcar.miniprojek.controller;

import com.rentcar.miniprojek.dto.login.LoginRequest;
import com.rentcar.miniprojek.dto.login.LoginResponse;
import com.rentcar.miniprojek.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/login")
@Validated
public class LoginController {

    private final UserService userService;
    @Autowired
    public LoginController(UserService userService){

        this.userService = userService;
    }

    @PostMapping("/signin")

    public ResponseEntity<LoginResponse> login(@Valid @RequestBody LoginRequest request) throws Exception {
        LoginResponse response = userService.login(request);
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/{userId}")
    public ResponseEntity<Void> deleteCustomer(@PathVariable("userId") Integer userId) {
        userService.deleteCustomer(userId);
        return ResponseEntity.noContent().build();
    }

}


