package com.rentcar.miniprojek.controller;

import com.rentcar.miniprojek.dto.vehicle.VehicleRequest;
import com.rentcar.miniprojek.dto.vehicle.VehicleResponse;
import com.rentcar.miniprojek.service.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {

    private final VehicleService vehicleService;
    @Autowired
    public VehicleController(VehicleService vehicleService){

        this.vehicleService = vehicleService;
    }

    @PostMapping("/add")
    public ResponseEntity<VehicleResponse> addVehicle(@RequestBody VehicleRequest vehicleRequest){
        VehicleResponse vehicleResponse = vehicleService.saveVehicle(vehicleRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(vehicleResponse);
    }

    @GetMapping("/all")
    public ResponseEntity<List<VehicleResponse>> getCategoryAll(){
        List<VehicleResponse> response = vehicleService.getAllVehicle();
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<VehicleResponse> putVehicle(@PathVariable("id") Integer id,@RequestBody VehicleRequest request){
        VehicleResponse response = vehicleService.updateVehicle(id, request);
        return ResponseEntity.ok(response);
    }

}