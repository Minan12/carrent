package com.rentcar.miniprojek.dto;

import lombok.Data;

@Data
public class BaseResponse<T> {
    private T data;
    private Boolean success = Boolean.TRUE;
    private String message = "Operation Success";

    public BaseResponse(T data){
        this.data = data;
    }
}
