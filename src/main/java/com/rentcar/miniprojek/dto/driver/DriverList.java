package com.rentcar.miniprojek.dto.driver;

import com.rentcar.miniprojek.entity.Driver;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

import java.util.List;

@Data
public class DriverList {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String driverName;
    private String drivingLicence;
    private List<Driver> drivers;
}
