package com.rentcar.miniprojek.dto.driver;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
public class DriverRequest {
    private String driverName;
    private String drivinglicence;
    private BigDecimal driverPrice;
}
