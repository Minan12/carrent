package com.rentcar.miniprojek.dto.login;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginRequest {
    @NotBlank(message = "Silahkan masukkan username")
    private String username;
    @NotBlank(message = "Silahkan masukkan Password ")
    private String password;
}
