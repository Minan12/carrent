package com.rentcar.miniprojek.dto.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class OrderDetail {
    private String custName;
    private String contactNo;
    private String address;
    private List<OrderList> orderList;
}
