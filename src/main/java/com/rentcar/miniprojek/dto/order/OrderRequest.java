package com.rentcar.miniprojek.dto.order;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OrderRequest {
    private Integer customerId;
    private Integer vehicleId;
    private Integer driverId;
    private Integer hours;
}
