package com.rentcar.miniprojek.dto.order;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class OrderResponse {
    private Integer id;
    private String custName;
    private String driverName;
    private String carName;
    private BigDecimal priceCar;
    private Integer hours;
    private BigDecimal totalPrice;
    private String status;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date tanggalOrder;
}
