package com.rentcar.miniprojek.dto.token;

import lombok.Data;

@Data
public class TokenResponse {
    private boolean isValid;
    private String role;
}
