package com.rentcar.miniprojek.dto.vehicle;

import com.rentcar.miniprojek.entity.Vehicle;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class VehicleList {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String carName;
    private String platNo;
    private BigDecimal priceCar;
    private String imgUrl;
    private List<Vehicle> vehicles;
}
