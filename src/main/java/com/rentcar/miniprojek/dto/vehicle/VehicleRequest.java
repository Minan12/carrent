package com.rentcar.miniprojek.dto.vehicle;

import lombok.*;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class VehicleRequest {
    private String carName;
    private String platNo;
    private BigDecimal priceCar;
    private String imgUrl;
}
