package com.rentcar.miniprojek.dto.vehicle;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class VehicleResponse {
    private Integer id;
    private String carName;
    private String platNo;
    private BigDecimal priceCar;
    private String imgUrl;
    private String status;
}
