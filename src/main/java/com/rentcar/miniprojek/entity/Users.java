package com.rentcar.miniprojek.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Data
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String username;
    private String password;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private Date tanggalLogin;


    @Column(length = 500)
    private String token;

    @OneToOne
    private Customer customer;

}
