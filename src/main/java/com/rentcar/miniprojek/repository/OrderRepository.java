package com.rentcar.miniprojek.repository;

import com.rentcar.miniprojek.dto.order.OrderList;
import com.rentcar.miniprojek.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
@Query("SELECT new com.rentcar.miniprojek.dto.order.OrderList(c.custName, d.driverName, v.carName, v.priceCar, o.hours, o.totalPrice) FROM Order o JOIN o.customer c JOIN o.vehicle v JOIN o.driver d WHERE c.id=:id")
    List<OrderList> findDetailOrder(@Param("id") Integer id);
}
