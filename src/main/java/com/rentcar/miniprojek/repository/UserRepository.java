package com.rentcar.miniprojek.repository;

import com.rentcar.miniprojek.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {
    Optional<Users> findByUsername(String username);
    Optional<Users> findByToken(String token);
    Optional<Users> findByUsernameAndPassword(String username, String password);



}
