package com.rentcar.miniprojek.service;

import com.rentcar.miniprojek.Exception.RegistrationException;
import com.rentcar.miniprojek.Exception.ResourceNotFoundException;
import com.rentcar.miniprojek.dto.customer.CustomerRequest;
import com.rentcar.miniprojek.dto.customer.CustomerResponse;
import com.rentcar.miniprojek.entity.Customer;
import com.rentcar.miniprojek.entity.Users;
import com.rentcar.miniprojek.repository.CustomerRepository;
import com.rentcar.miniprojek.repository.UserRepository;
import com.rentcar.miniprojek.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.Optional;
import lombok.SneakyThrows;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private static final String CUSTOMER_DOES_NOT_EXIST = "customer_does_not_exist";
    private final CustomerRepository customerRepository;
    private final UserRepository userRepository;
    private final CustomerValidationService customerValidationService;
    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public Customer customerExistValidation(Integer customerId){
        Customer customerExist = customerRepository.findById(customerId)
                .orElseThrow(()-> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, CUSTOMER_DOES_NOT_EXIST, customerId)));
        return customerExist;
    }


    public CustomerResponse saveCustomer(CustomerRequest request)throws RegistrationException {
        customerValidationService.validateCustomer(request);

        Customer customer = new Customer();
        Users users = new Users();

        customer.setCustName(request.getCustName());
        customer.setAddress(request.getAddress());
        customer.setContactNo(request.getContactNo());
        customer.setDrivingLicence(request.getDrivingLicence());
        customer.setUsername(request.getUsername());
        customer.setPassword(request.getPassword());
        customerRepository.save(customer);

        users.setCustomer(customer);
        users.setPassword(customer.getPassword());
        users.setUsername(customer.getUsername());

        userRepository.save(users);

        return CustomerResponse.builder()
                .id(customer.getId())
                .custName(customer.getCustName())
                .address(customer.getAddress())
                .contactNo(customer.getContactNo())
                .drivingLicence(customer.getDrivingLicence())
                .username(customer.getUsername())
                .password(customer.getPassword())
                .message("Create Success")
                .build();

    }

    @SneakyThrows
    public CustomerResponse updateCustomer(Integer id, CustomerRequest request) {
        Customer cus = customerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("User not found"));

        Users l = new Users();

        cus.setId(id);
        cus.setCustName(request.getCustName());
        cus.setAddress(request.getAddress());
        cus.setContactNo(request.getContactNo());
        cus.setDrivingLicence(request.getDrivingLicence());
        l.setUsername(request.getUsername());
        l.setPassword(request.getPassword());

        customerRepository.save(cus);

        return CustomerResponse.builder()
                .message("Update success")
                .id(cus.getId())
                .custName(cus.getCustName())
                .address(cus.getAddress())
                .contactNo(cus.getContactNo())
                .drivingLicence(cus.getDrivingLicence())
                .username(cus.getUsername())
                .password(cus.getPassword())
                .build();
    }



    @SneakyThrows
    public CustomerResponse getById(Integer id) {
        Optional<Customer> listCustomer = customerRepository.findById(id);

        if (!listCustomer.isPresent()) {
            throw new IllegalArgumentException();
        }
        Customer c = listCustomer.get();

        return CustomerResponse.builder()
                .id(c.getId())
                .custName(c.getCustName())
                .address(c.getAddress())
                .contactNo(c.getContactNo())
                .drivingLicence(c.getDrivingLicence())
                .username(c.getUsername())
                .password(c.getPassword())
                .build();
    }


}
