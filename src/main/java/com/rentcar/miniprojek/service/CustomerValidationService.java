package com.rentcar.miniprojek.service;

import com.rentcar.miniprojek.Exception.RegistrationException;
import com.rentcar.miniprojek.dto.customer.CustomerRequest;
import com.rentcar.miniprojek.repository.CustomerRepository;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Builder
@Service
@RequiredArgsConstructor
public class CustomerValidationService {

    public static final String USERNAME_ALREADY_EXISTS = "Username already exists";
    public static final String NOHP_ALREADY_EXISTS = "NoHp already exists";
    public static final String EMPTY_FIELD_MESSAGE = "Please fill in %s customer";

    private final CustomerRepository customerRepository;

    public void validateCustomer(CustomerRequest request) {
        List<String> errorMessages = new ArrayList<>();

        final String custName = request.getCustName();
        final String address = request.getAddress();
        final String contactNo = request.getContactNo();
        final String drivingLicence = request.getDrivingLicence();
        final String username = request.getUsername();
        final String password = request.getPassword();

        checkFieldNotEmpty(custName, "name", errorMessages);
        checkFieldNotEmpty(address, "address", errorMessages);
        checkFieldNotEmpty(contactNo, "No Contact", errorMessages);
        checkFieldNotEmpty(drivingLicence, "Driver License", errorMessages);
        checkFieldNotEmpty(username, "username", errorMessages);
        checkFieldNotEmpty(password, "password", errorMessages);

        checkUsername(username, errorMessages);
        checkContactNo(contactNo, errorMessages);

        if (!errorMessages.isEmpty()) {
            throw new RegistrationException(errorMessages);
        }
    }

    private void checkFieldNotEmpty(String value, String fieldName, List<String> errorMessages) {
        if (value == null || value.trim().isEmpty()) {
            errorMessages.add(String.format(EMPTY_FIELD_MESSAGE, fieldName));
        }
    }

    private void checkUsername(String username, List<String> errorMessages) {
        final boolean existsByUsername = customerRepository.existsByUsername(username);

        if (existsByUsername) {
            errorMessages.add(USERNAME_ALREADY_EXISTS);
        }
    }

    private void checkContactNo(String contactNo, List<String> errorMessages) {
        final boolean existsByContactNo = customerRepository.existsByContactNo(contactNo);

        if (existsByContactNo) {
            errorMessages.add(NOHP_ALREADY_EXISTS);
        }
    }
}
