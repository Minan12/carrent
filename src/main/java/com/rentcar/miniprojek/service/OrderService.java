package com.rentcar.miniprojek.service;

import com.rentcar.miniprojek.Exception.BadRequestException;
import com.rentcar.miniprojek.Exception.ResourceNotFoundException;
import com.rentcar.miniprojek.dto.order.OrderDetail;
import com.rentcar.miniprojek.dto.order.OrderList;
import com.rentcar.miniprojek.dto.order.OrderRequest;
import com.rentcar.miniprojek.dto.order.OrderResponse;
import com.rentcar.miniprojek.entity.Customer;
import com.rentcar.miniprojek.entity.Driver;
import com.rentcar.miniprojek.entity.Order;
import com.rentcar.miniprojek.entity.Vehicle;
import com.rentcar.miniprojek.repository.CustomerRepository;
import com.rentcar.miniprojek.repository.DriverRepository;
import com.rentcar.miniprojek.repository.OrderRepository;
import com.rentcar.miniprojek.repository.VehicleRepository;
import com.rentcar.miniprojek.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrderService {
    private static final String ORDER_DOES_NOT_EXIST = "order_does_not_exist";
    private static final String STATUS_INORDER = "Inorder";
    private static final String STATUS_COMPLETE = "Complete";
    private final TokenService tokenService;
    private final CustomerRepository customerRepository;
    private final VehicleRepository vehicleRepository;
    private final OrderRepository orderRepository;
    private final DriverRepository driverRepository;
    private final DriverService driverService;
    private final VehicleService vehicleService;
    private final CustomerService customerService;
    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public Order orderExistValidation(Integer orderId) {
        Order orderExist = orderRepository.findById(orderId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, ORDER_DOES_NOT_EXIST, orderId)));
        return orderExist;
    }


    public OrderResponse addOrder(String token,OrderRequest request){
        if (!tokenService.getToken(token)){
            throw new BadRequestException("token tidak ada");
        }

        Vehicle vehicle = vehicleRepository.findById(request.getVehicleId()).orElseThrow(() -> new EntityNotFoundException("Vehicle Id Not Found" + request.getVehicleId() + "vehicle not found"));
        Customer customer = customerRepository.findById(request.getCustomerId()).orElseThrow(() -> new EntityNotFoundException("Customer Id Not Found" + request.getCustomerId() + "customer not found"));
        Driver driver = driverRepository.findById(request.getDriverId()).orElseThrow(() -> new EntityNotFoundException("Driver Name Not Found" + request.getDriverId() + "driver not found"));

        if (vehicle.getStatus().equalsIgnoreCase(STATUS_INORDER)){
            throw new BadRequestException("vehicle tidak tersedia");
        }

        if (driver.getStatus().equalsIgnoreCase(STATUS_INORDER)){
            throw new BadRequestException("driver tidak tersedia");
        }

        Order order = new Order();
        order.setHours(request.getHours());
        order.setTanggalOrder(new Date());
        order.setCustomer(customer);
        order.setVehicle(vehicle);
        order.setDriver(driver);
        order.setTotalPrice(vehicle.getPriceCar().multiply(BigDecimal.valueOf(request.getHours())).add(driver.getDriverPrice()));
        order.setStatus(STATUS_INORDER);
        orderRepository.save(order);

        vehicle.setStatus(STATUS_INORDER);
        vehicleRepository.save(vehicle);
        driver.setStatus(STATUS_INORDER);
        driverRepository.save(driver);

        return OrderResponse.builder()
                .id(order.getId())
                .custName(customer.getCustName())
                .driverName(driver.getDriverName())
                .priceCar(vehicle.getPriceCar())
                .carName(vehicle.getCarName())
                .hours(order.getHours())
                .tanggalOrder(new Date())
                .status(order.getStatus())
                .totalPrice(order.getTotalPrice())
                .build();

    }

    public OrderResponse updateOrder(String token, Integer id, OrderRequest request){
        if (!tokenService.getToken(token)){
            throw new BadRequestException("token tidak ada");
        }

        Order order = orderExistValidation(id);
        Vehicle vehicleExist = vehicleService.vehicleExistValidation(request.getVehicleId());
        Driver driverExist = driverService.driverExistValidation(request.getDriverId());
        Customer customerExist = customerService.customerExistValidation(request.getCustomerId());

        order.setId(id);
        order.setCustomer(customerExist);
        order.setVehicle(vehicleExist);
        order.setDriver(driverExist);
        order.setHours(request.getHours());
        order.setTotalPrice(vehicleExist.getPriceCar().multiply(BigDecimal.valueOf(request.getHours())).add(driverExist.getDriverPrice()));
        order.setTanggalOrder(order.getTanggalOrder());
        order.setStatus(STATUS_COMPLETE);
        orderRepository.save(order);

        vehicleExist.setStatus(STATUS_COMPLETE);
        vehicleRepository.save(vehicleExist);
        driverExist.setStatus(STATUS_COMPLETE);
        driverRepository.save(driverExist);

        return OrderResponse.builder()
                .id(order.getId())
                .custName(order.getCustomer().getCustName())
                .driverName(order.getDriver().getDriverName())
                .priceCar(order.getVehicle().getPriceCar())
                .carName(order.getDriver().getDriverName())
                .hours(order.getHours())
                .totalPrice(order.getTotalPrice())
                .tanggalOrder(order.getTanggalOrder())
                .status(order.getStatus())
                .build();
    }

    public OrderDetail getDetail(Integer customerId){

        Optional<Customer> optional = customerRepository.findById(customerId);
        if (!optional.isPresent()){
            return null;
        }

        Customer customer = optional.get();
        List<OrderList> orderList = orderRepository.findDetailOrder(customerId);

        return OrderDetail.builder()
                .custName(customer.getCustName())
                .address(customer.getAddress())
                .contactNo(customer.getContactNo())
                .orderList(orderList)
                .build();
    }

    public List<OrderResponse> getAllOrder(){
        List<OrderResponse> orderList = new ArrayList<>();

        for (Order order : orderRepository.findAll()){
            orderList.add(OrderResponse.builder()
                    .id(order.getId())
                    .custName(order.getCustomer().getCustName())
                    .driverName(order.getDriver().getDriverName())
                    .priceCar(order.getVehicle().getPriceCar())
                    .carName(order.getVehicle().getCarName())
                    .hours(order.getHours())
                    .totalPrice(order.getTotalPrice())
                    .tanggalOrder(order.getTanggalOrder())
                    .build()
            );

        }

        return orderList;
    }

}
