package com.rentcar.miniprojek.util;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Locale;
import java.util.Objects;

@Service
public class ExceptionMessageAccessor {
    private final MessageSource messageSource;


    public ExceptionMessageAccessor(@Qualifier("exceptionMessageSource") MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getMessage(Locale locale, String key, Object... parameter){
        if (Objects.isNull(locale)){
            return messageSource.getMessage(key, parameter, ProjectConstant.ID_LOCALE);
        }

        return messageSource.getMessage(key, parameter, locale);
    }
}
