package com.rentcar.miniprojek.util;

import com.rentcar.miniprojek.dto.login.LoginRequest;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.crypto.MacProvider;

import java.security.Key;
import java.util.Date;

public class JwToken {

    public static String generateToken(LoginRequest request) {
        try {
            long currentMillis = System.currentTimeMillis();
            Date now = new Date(currentMillis);
            Key key = MacProvider.generateKey();

            return Jwts.builder()
                    .setSubject(String.valueOf(request))
                    .setSubject("users")
                    .setIssuedAt(now)
                    .signWith(SignatureAlgorithm.HS512, key)
                    .compact();
        } catch (Exception e) {
            throw new IllegalArgumentException("Error generating token", e);
        }
    }
}