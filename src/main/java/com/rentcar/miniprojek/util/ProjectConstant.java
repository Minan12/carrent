package com.rentcar.miniprojek.util;

import java.util.Locale;

public class ProjectConstant {
    public static final String DEFAULT_ENCODING = "UTF-8";

    public static final String PROJECT_BASE_PACKAGE = "com.rentcar.miniprojek";

    public static final Locale ID_LOCALE = new Locale.Builder().build();

    private ProjectConstant() {

        throw new UnsupportedOperationException();
    }

}
