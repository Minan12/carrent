package com.rentcar.miniprojek.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentcar.miniprojek.Exception.RegistrationException;
import com.rentcar.miniprojek.dto.customer.CustomerRequest;
import com.rentcar.miniprojek.dto.customer.CustomerResponse;
import com.rentcar.miniprojek.entity.Customer;
import com.rentcar.miniprojek.service.CustomerService;
import com.rentcar.miniprojek.service.CustomerValidationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CustomerControllerTest {
    @InjectMocks
    private CustomerController customerController;
    @Mock
    private CustomerService customerService;
    @Mock
    private CustomerValidationService customerValidationService;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void doPositiveAddCustomer() throws RegistrationException {
        CustomerRequest request = CustomerRequest.builder()
                .custName("hannis")
                .address("bekasi")
                .contactNo("0987")
                .drivingLicence("c")
                .username("hannis1")
                .password("1234")
                .build();

        customerValidationService.validateCustomer(request);

        Customer customer = new Customer();
        customer.setId(1);
        customer.setCustName(request.getCustName());
        customer.setAddress(request.getAddress());
        customer.setContactNo(request.getContactNo());
        customer.setDrivingLicence(request.getDrivingLicence());
        customer.setUsername(request.getUsername());
        customer.setPassword(request.getPassword());

        CustomerResponse response = CustomerResponse.builder()
                .custName("hannis")
                .address("bekasi")
                .contactNo("0987")
                .drivingLicence("c")
                .username("hannis1")
                .password("1234")
                .build();

        when(customerService.saveCustomer(request))
                .thenReturn(response);
        ResponseEntity<CustomerResponse> responseEntity = customerController.addCustomer(request);
        Assertions.assertSame(HttpStatus.CREATED, responseEntity.getStatusCode());

    }

    @Test
    void doNegativeAddCustomer() throws RegistrationException {
        CustomerRequest request = CustomerRequest.builder()
                .custName("hannis")
                .address("bekasi")
                .contactNo("0987")
                .drivingLicence("c")
                .username("hannis1")
                .password("1234")
                .build();

        customerService = Mockito.mock(CustomerService.class);
        doThrow(RegistrationException.class).when(customerService).saveCustomer(request);
        try {
            customerService.saveCustomer(request);
            Assertions.fail("Username already exists");
        } catch (RegistrationException e) {
            Assertions.assertEquals("Username already exists", CustomerValidationService.USERNAME_ALREADY_EXISTS);
        }
    }

    @Test
    void doNegativeUpdateCustomer() {
        CustomerRequest request = CustomerRequest.builder()
                .custName("hannis")
                .address("bekasi")
                .contactNo("0987")
                .drivingLicence("c")
                .username("hannis1")
                .password("1234")
                .build();

        customerService = Mockito.mock(CustomerService.class);
        doThrow(RegistrationException.class).when(customerService).saveCustomer(request);
        try {
            customerService.saveCustomer(request);
            Assertions.fail("Username already exists");
        }catch (RegistrationException e){
            Assertions.assertEquals("Username already exists",CustomerValidationService.USERNAME_ALREADY_EXISTS);
        }
    }

}

