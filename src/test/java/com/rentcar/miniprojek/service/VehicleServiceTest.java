package com.rentcar.miniprojek.service;

import com.rentcar.miniprojek.dto.vehicle.VehicleRequest;
import com.rentcar.miniprojek.dto.vehicle.VehicleResponse;
import com.rentcar.miniprojek.entity.Vehicle;
import com.rentcar.miniprojek.repository.VehicleRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class VehicleServiceTest {

    @Mock
    private VehicleRepository vehicleRepository;

    @InjectMocks
    private VehicleService vehicleService;

    @Test
    void testSaveVehicle() {
        // Arrange
        VehicleRequest request = new VehicleRequest("Car1", "ABC123", BigDecimal.valueOf(100), "image1");

        Vehicle savedVehicle = new Vehicle();
        savedVehicle.setCarName("Car1");
        savedVehicle.setPlatNo("ABC123");
        savedVehicle.setPriceCar(BigDecimal.valueOf(100));
        savedVehicle.setImgUrl("image1");

        when(vehicleRepository.save(any(Vehicle.class))).thenReturn(savedVehicle);

        // Act
        VehicleResponse result = vehicleService.saveVehicle(request);

        // Assert
        assertEquals("Car1", result.getCarName());
        assertEquals("ABC123", result.getPlatNo());
        assertEquals(BigDecimal.valueOf(100), result.getPriceCar());
        assertEquals("image1", result.getImgUrl());

        // Verify that necessary methods were called
        verify(vehicleRepository, times(1)).save(any(Vehicle.class));
    }

    @Test
    void testUpdateVehiclePositive() {
        // Arrange
        Integer vehicleId = 1;
        VehicleRequest vehicleRequest = new VehicleRequest("UpdatedCar", "XYZ789", BigDecimal.valueOf(150), "updated-image");

        Vehicle existingVehicle = new Vehicle();
        existingVehicle.setId(vehicleId);
        existingVehicle.setCarName("OldCar");
        existingVehicle.setPlatNo("OLD123");
        existingVehicle.setPriceCar(BigDecimal.valueOf(120));
        existingVehicle.setImgUrl("old-image-url");

        when(vehicleRepository.findById(vehicleId)).thenReturn(Optional.of(existingVehicle));
        when(vehicleRepository.save(any(Vehicle.class))).thenAnswer(invocation -> invocation.getArgument(0));

        // Act
        VehicleResponse result = vehicleService.updateVehicle(vehicleId, vehicleRequest);

        // Assert
        assertNotNull(result);
        assertEquals(vehicleId, result.getId());
        assertEquals(vehicleRequest.getCarName(), result.getCarName());
        assertEquals(vehicleRequest.getPlatNo(), result.getPlatNo());
        assertEquals(vehicleRequest.getPriceCar(), result.getPriceCar());
        assertEquals(vehicleRequest.getImgUrl(), result.getImgUrl());

        verify(vehicleRepository, times(1)).findById(vehicleId);
        verify(vehicleRepository, times(1)).save(any(Vehicle.class));
    }

    @Test
    void testGetAllVehicle() {
        // Arrange
        List<Vehicle> mockVehicleList = new ArrayList<>();

        Vehicle vehicle1 = new Vehicle();
        vehicle1.setId(1);
        vehicle1.setCarName("Car1");
        vehicle1.setPlatNo("ABC123");
        vehicle1.setPriceCar(BigDecimal.valueOf(100));
        vehicle1.setImgUrl("image1");

        Vehicle vehicle2 = new Vehicle();
        vehicle2.setId(2);
        vehicle2.setCarName("Car2");
        vehicle2.setPlatNo("XYZ789");
        vehicle2.setPriceCar(BigDecimal.valueOf(150));
        vehicle2.setImgUrl("image2");

        mockVehicleList.add(vehicle1);
        mockVehicleList.add(vehicle2);

        when(vehicleRepository.findAll()).thenReturn(mockVehicleList);

        // Act
        List<VehicleResponse> result = vehicleService.getAllVehicle();

        // Assert
        assertEquals(2, result.size());

        // Verify that necessary methods were called
        verify(vehicleRepository, times(1)).findAll();
    }
}